import c4d

REBOOTME_PLUGIN_ID = 1061285

class RebootmeCommand(c4d.plugins.CommandData):
	def Execute(self, doc):
		return c4d.RestartMe()

def main():
	c4d.plugins.RegisterCommandPlugin(id=REBOOTME_PLUGIN_ID, str="Reboot", info=0, help="Restart C4D", dat=RebootmeCommand(), icon=None)

if __name__ == "__main__":
	main()