import c4d, maxon, os

TEMPLATE_L4D_FILEPATH = "../../prefs/template.l4d"

AUTOLOAD_STARTUP_LAYOUT_PLUGIN_ID = 1062179
UPDATE_STARTUP_LAYOUT_PLUGIN_ID = 1062180

def GetSharedPrefsFolder():
	basePlugin = c4d.plugins.FindPlugin(AUTOLOAD_STARTUP_LAYOUT_PLUGIN_ID)
	if not basePlugin:
		return None
	pluginFile = basePlugin.GetFilename()
	if not pluginFile:
		return None
	return os.path.abspath(os.path.join(os.path.dirname(pluginFile), TEMPLATE_L4D_FILEPATH))

class AutoloadL4DCommand(c4d.plugins.CommandData):
	def Execute(self, doc):
		template_l4d_filepath = GetSharedPrefsFolder()
		if not template_l4d_filepath:
			c4d.gui.MessageDialog(f"Couldn't retrieve source template.l4d file path");
			return True
		newTemplateUrl: maxon.Url = maxon.Url(template_l4d_filepath)
		if newTemplateUrl.IoDetect() != maxon.IODETECT.FILE:
			c4d.gui.MessageDialog(f"Couldn't find template.l4d. The following path doesn't exist: {prefsUrl}")
			return True
		prefsUrl: maxon.Url = maxon.Application.GetUrl(maxon.APPLICATION_URLTYPE.PREFS_DIR) + "prefs"
		if prefsUrl.IoDetect() != maxon.IODETECT.DIRECTORY:
			c4d.gui.MessageDialog(f"Couldn't find prefs directory. The following path doesn't exist: {prefsUrl}")
			return True
		existingTemplateUrl: maxon.Url = prefsUrl + "template.l4d"
		if existingTemplateUrl.IoDetect() == maxon.IODETECT.FILE:
			if c4d.gui.MessageDialog(f"template.l4d already exists. Do you want to override it?", c4d.GEMB_YESNO) == c4d.GEMB_R_V_NO:
				return True
			
		print(f"Copying {newTemplateUrl} to {existingTemplateUrl}")
		newTemplateUrl.IoCopyFile(existingTemplateUrl, True, False)

		if existingTemplateUrl.IoDetect() == maxon.IODETECT.FILE:
			# if c4d.gui.MessageDialog(f"Do you want to restart C4D?", c4d.GEMB_YESNO) == c4d.GEMB_R_V_YES:
			# 	return c4d.RestartMe()
			if c4d.gui.MessageDialog(f"Do you want to load this layout?", c4d.GEMB_YESNO) == c4d.GEMB_R_V_YES:
				c4d.documents.LoadFile(existingTemplateUrl)
		else:
			c4d.gui.MessageDialog(f"Couldn't copy the file, please check your folders or do it manually!")
		return True
	
class UpdateL4DCommand(c4d.plugins.CommandData):
	def Execute(self, doc):
		prefsUrl: maxon.Url = maxon.Application.GetUrl(maxon.APPLICATION_URLTYPE.PREFS_DIR) + "prefs"
		if prefsUrl.IoDetect() != maxon.IODETECT.DIRECTORY:
			c4d.gui.MessageDialog(f"Couldn't find prefs directory. The following path doesn't exist: {prefsUrl}")
			return True
		existingTemplateUrl: maxon.Url = prefsUrl + "template.l4d"
		if existingTemplateUrl.IoDetect() == maxon.IODETECT.NONEXISTENT:
			c4d.gui.MessageDialog(f"template.l4d doesn't exist")
			return True
		template_l4d_filepath = GetSharedPrefsFolder()
		if not template_l4d_filepath:
			c4d.gui.MessageDialog(f"Couldn't retrieve target template.l4d file path");
			return True
		newTemplateUrl: maxon.Url = maxon.Url(template_l4d_filepath)
		if newTemplateUrl.IoDetect() == maxon.IODETECT.FILE:
			if c4d.gui.MessageDialog(f"{newTemplateUrl} already exists. Do you want to override it?", c4d.GEMB_YESNO) == c4d.GEMB_R_V_NO:
				return True
			
		print(f"Copying {existingTemplateUrl} to {newTemplateUrl}")
		existingTemplateUrl.IoCopyFile(newTemplateUrl, True, False)

		c4d.gui.MessageDialog("Done!")
		return True

def main():
	c4d.plugins.RegisterCommandPlugin(id=AUTOLOAD_STARTUP_LAYOUT_PLUGIN_ID, str="Autoload template.l4d", info=0, help="Copy template.l4d over to startup layout and reboot", dat=AutoloadL4DCommand(), icon=None)
	c4d.plugins.RegisterCommandPlugin(id=UPDATE_STARTUP_LAYOUT_PLUGIN_ID, str="Export template.l4d", info=0, help="Copy template.l4d over from startup layout", dat=UpdateL4DCommand(), icon=None)

if __name__ == "__main__":
	main()