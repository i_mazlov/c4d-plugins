# Some quality of life plugins for C4D

### Autoload l4d
Creates (or replaces) the existing template.l4d into Cinema4D with one click

#### Getting Started
Place your template.l4d file somewhere around and change the filepath to it in autoloadl4d.pyp file under ```TEMPLATE_L4D_FILEPATH``` variable

### Reboot
Single click reboot

## Expose plugins for all C4D versions at once
Add a global environment variable:

```g_additionalModulePath=<path_to_your_plugins_folder>```